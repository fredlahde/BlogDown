<h1 class="text-center"><a href="{{ url('/') }}">{{ config('app.name') }}</a></h1>
<h2 class="text-center">{{ env('APP_SUBTITLE', 'Subtitle') }}</h2>