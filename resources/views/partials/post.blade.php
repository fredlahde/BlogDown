<div class="post panel panel-default">
    <div class="panel-heading">
        <span>
            {{ $post->updated_at->diffForHumans() }}
        </span>
        <span>
            <a href="{{ url('/post/' . $post->slug) }}">Permalink</a>
        </span>
    </div>
    <div class="panel-body">

        {!! $post->html !!}
    </div>
</div>