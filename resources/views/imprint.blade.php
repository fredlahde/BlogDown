@extends('partials.layout')
@section('content')
    <div class="post panel panel-default">
        <div class="panel-heading">
        <span>
           Imprint
        </span>
        </div>
        <div class="panel-body">
            {!! $imprint !!}
        </div>
    </div>

@endsection