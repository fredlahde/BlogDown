# BlogDown - blogging for hackers

This is the code that runs [z3dy.com](http://z3dy.com)

It takes .md files, parses them into HTML and builds a blog out of it.

## Set up
First of all, you need to setup a few dependencies:

    composer install

Next set up .env file with:

    cp .env.example .env
    php artisan key:generate

Then, set APP_NAME and APP_SUBTITLE accordingly. 
You can decide whether to show the Footer with APP_FOOTER.

Next, set up your Database and run

    php artisan migrate

Now, everything should be set up and you can start writing!

## Commands

### Query

    php artisan query

Query the storage/markdown directory for new markdown files, parse them and add them to the Database

### Update a specific post

    php artisan update {filename}
    
Update the post with the specified filename
