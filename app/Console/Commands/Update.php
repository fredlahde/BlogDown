<?php

namespace App\Console\Commands;

use App\Post;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\File;
use Parsedown;

class Update extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'update {filename}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update the specified post';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $parser = new Parsedown();
        $post = Post::where('filename', '=', $this->argument('filename'))->first();

        $markdown = File::get(storage_path('markdown') .DIRECTORY_SEPARATOR . $post->filename);
        $post->html = $parser->parse($markdown);

        $post->save();
    }
}
