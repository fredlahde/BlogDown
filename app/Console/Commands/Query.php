<?php

namespace App\Console\Commands;

use App\Post;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\File;
use Parsedown;

class Query extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'query';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Query the markdown directory and update the db';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $parser = new Parsedown();
        $files = collect(File::allFiles(storage_path('markdown')));

        $files->filter(function($file, $key) {
            return $file->getExtension() == 'md';
        })->filter(function ($file, $key) {
            return !Post::where('filename', '=', $file->getFilename())->exists();
        })->filter(function ($file, $key) {
            return $file->getFilename() != 'imprint.md';
        })->each(function ($file, $key) use ($parser) {
            Post::create([
                'slug' => str_random(6),
                'filename' => $file->getFilename(),
                'html' => $parser->parse(File::get($file->getPathname()))
            ]);

            $this->info('Parsed: ' . $file->getFilename());
        });

        return true;
    }
}
