<?php

namespace App\Http\Controllers;

use App\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;


class HomeController extends Controller
{
    public function index(Request $request)
    {
        $posts = Post::all();

        return view('welcome')->with(compact('posts'));
    }

    public function show(Request $request, $slug)
    {
        $post = Post::where('slug', '=', $slug);

        if ($post->first()) {
            return view('showPost')->with('post', $post->first());
        }
        throw new NotFoundHttpException();
    }

    public function imprint(Request $request)
    {
        $parser = new \Parsedown();

        $imprint = $parser->parse(File::get(storage_path('markdown') . DIRECTORY_SEPARATOR . 'imprint.md'));

        return view('imprint')->with(compact('imprint'));
    }
}
